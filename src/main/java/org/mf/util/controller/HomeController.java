package org.mf.util.controller;

import java.io.IOException;
import java.util.List;

import org.mf.util.model.Fund;
import org.mf.util.model.Stock;
import org.mf.util.repo.FundRepo;
import org.mf.util.repo.PortfolioRepo;
import org.mf.util.repo.StockRepo;
import org.mf.util.service.FetchService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@RequiredArgsConstructor
public class HomeController {

    private final FetchService fetchService;
    private final FundRepo fundRepo;
    private final StockRepo stockRepo;
    private final PortfolioRepo portfolioRepo;

    @GetMapping("/")
    public String home(Model model) {
        List<Object[]> mfData = fundRepo.getMfByAsset();
        List<Object[]> stockData = stockRepo.getStockByAsset();
        model.addAttribute("mfData", mfData);
        model.addAttribute("stockData", stockData);
        return "home";
    }

    @GetMapping("/fund-all/{type}")
    public String getAllMutualFundByType(@PathVariable String type, Model model) {
        List<Fund> fundList = fundRepo.getFundByType(type);
        model.addAttribute("fundList", fundList);
        model.addAttribute("type", type);
        return "fund-all";
    }

    @GetMapping("/fund-all")
    public String getAllMutualFund(Model model) {
        List<Fund> fundList = fundRepo.findAllLatest();
        model.addAttribute("fundList", fundList);
        model.addAttribute("type", "ALL");
        return "fund-all";
    }

    @GetMapping("/stock-all/{type}")
    public String getAllStockByType(@PathVariable String type, Model model) {
        List<Stock> stockList = stockRepo.getStockByType(type);
        model.addAttribute("stockList", stockList);
        model.addAttribute("type", type);
        return "stock-all";
    }

    @GetMapping("/stock-all")
    public String getAllStock(Model model) {
        Iterable<Stock> stockList = stockRepo.findAllLatest();
        model.addAttribute("stockList", stockList);
        model.addAttribute("type", "ALL");
        return "stock-all";
    }

    @GetMapping("/stock/{stockId}")
    public String getStock(@PathVariable String stockId, Model model) {
        Stock stock = stockRepo.findByStockId(stockId);
        List<Object[]> fundData = portfolioRepo.getAllByStockId(stockId);
        model.addAttribute("stock", stock);
        model.addAttribute("fundData", fundData);
        return "stock";
    }

    @GetMapping("/pull-mf")
    public String pullMutualFundData() throws IOException {
        fetchService.pullMutualFundData();
        return "redirect:/";
    }

    @GetMapping("/pull-stock")
    public String pullStockData() throws IOException {
        fetchService.pullStockData();
        return "redirect:/";
    }

    @GetMapping("/pull-mf-portfolio")
    public String pullPortfolioData() throws IOException {
        fetchService.pullPortfolioData();
        return "redirect:/";
    }

    @GetMapping("/link-mf-stock")
    public String linkFundAndStock() throws IOException {
        fetchService.linkFundAndStock();
        return "redirect:/";
    }
}
