package org.mf.util.repo;

import java.util.Date;
import java.util.List;

import org.mf.util.model.Fund;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface FundRepo extends CrudRepository<Fund, Long> {

  @Query(value = "select count(*) from fund where cast(onday as date) = CURRENT_DATE", nativeQuery = true)
  Integer checkIfLoaded();

  @Query(value = "select sum(asset) as asset,category from fund where cast(onday as date) = " +
              "(select max(cast(onday as date)) from fund) group by category order by asset desc", nativeQuery = true)
  List<Object[]> getMfByAsset();
  
  @Query(value = "SELECT * FROM fund where category = ?1 and cast(onday as date) = (select max(cast(onday as date)) from fund) order by " +
              "asset desc", nativeQuery = true)
  List<Fund> getFundByType(String type);

  @Query(value = "select * from fund where cast(onday as date) = (select max(cast(onday as date)) from fund) order by" +
          " asset desc",
          nativeQuery = true)
  List<Fund> findAllLatest();

  Fund getFundByFundId(String fundId);
}
