package org.mf.util.repo;

import java.util.Date;
import java.util.List;

import org.mf.util.model.Portfolio;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface PortfolioRepo extends CrudRepository<Portfolio,Long> {
    @Query(value = "select count(*) from portfolio where cast(onday as date) = CURRENT_DATE", nativeQuery = true)
    Integer checkIfLoaded();

    @Query(value = "select (b.asset*a.allocation/100) investment,a.fund,b.asset,a.allocation,b.category, a.stock_id, " +
            "a.onday " +
            "from " +
            "portfolio a, fund b " +
            "where a.fund_id = b.fund_id " +
            "and cast(a.onday as date) = cast(b.onday as date) " +
            "and cast(a.onday as date) = (select max(cast(onday as date)) from portfolio) " +
            "and a.stock_id = ?1 " +
            "order by investment desc", nativeQuery = true)
    List<Object[]> getAllByStockId(String stockId);

    @Query(value = "select * from portfolio where cast(onday as date) = (select max(cast(onday as date)) from portfolio) ",
            nativeQuery = true)
    List<Portfolio> findAllLatest();
}
