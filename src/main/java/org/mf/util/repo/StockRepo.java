package org.mf.util.repo;

import java.util.Date;
import java.util.List;

import org.mf.util.model.Stock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface StockRepo extends CrudRepository<Stock, Long> {

    @Query(value = "select count(*) from stock where cast(onday as date) = CURRENT_DATE", nativeQuery = true)
    Integer checkIfLoaded();

    @Query(value = "SELECT * FROM stock where category = ?1 and cast(onday as date) = (select max(cast(onday as date)) from stock) order" +
            " by (mcap) desc", nativeQuery = true)
    List<Stock> getStockByType(String type);

    @Query(value = "select sum(mcap) as asset,category from stock where cast(onday as date) = " +
            "(select max(cast(onday as date)) from stock) group by category order by asset desc", nativeQuery = true)
    List<Object[]> getStockByAsset();

    @Query(value = "select * from stock where stock_id = ?1 and cast(onday as date) = " +
            "(select max(cast(onday as date)) from stock)", nativeQuery = true)
    Stock findByStockId(String stockId);

    @Query(value = "select * from stock where cast(onday as date) = (select max(cast(onday as date)) from stock) " +
            "order by mcap desc",
            nativeQuery = true)
    List<Stock> findAllLatest();
}
