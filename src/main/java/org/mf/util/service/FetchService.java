package org.mf.util.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.mf.util.model.Fund;
import org.mf.util.model.Portfolio;
import org.mf.util.model.Stock;
import org.mf.util.repo.FundRepo;
import org.mf.util.repo.PortfolioRepo;
import org.mf.util.repo.StockRepo;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
public class FetchService {
    private static final ObjectMapper mapper = new ObjectMapper();
    private final String HOST = "https://www.valueresearchonline.com";
    private final String FUNDS_BASE_URL = "https://www.valueresearchonline.com/funds/selector-data";
    private final String STOCKS_BASE_URL = "https://www.valueresearchonline.com/stocks/selector-data";
    private final String FILTER = "?end-type=1&plan-type=direct&exclude=suspended-plans&tab=snapshot&output=html-data";
    private final String FILTER_2 = "?output=html-data&draw=2&start=0&length=1500";
    private final FundRepo fundRepo;
    private final StockRepo stockRepo;
    private final PortfolioRepo portfolioRepo;

    @Async
    @Transactional
    public void pullMutualFundData() throws IOException {
        if (fundRepo.checkIfLoaded() > 0) {
            log.info("MF data already present!");
            return;
        }
        //all debt
        pullMutualFundData(FUNDS_BASE_URL + "/primary-category/2/debt/" + FILTER);
        //hybrid all.
        pullMutualFundData(FUNDS_BASE_URL + "/primary-category/3/hybrid/" + FILTER);
        //gold
        pullMutualFundData(FUNDS_BASE_URL + "/category/144/commodities-gold/" + FILTER);
        //equity funds
        pullMutualFundData(FUNDS_BASE_URL + "/primary-category/1/equity/" + FILTER);
        log.info("Completed!");
    }

    @Async
    @Transactional
    public void pullPortfolioData() throws IOException {
        if (portfolioRepo.checkIfLoaded() > 0) {
            log.info("Portfolio data already present!");
            return;
        }
        pullMutualFundPortfolioData();
        log.info("Completed!");
    }

    @Async
    @Transactional
    public void pullStockData() throws IOException {
        if(stockRepo.checkIfLoaded() > 0) {
            log.info("Stock data already present!");
            return;
        }
        pullStockData(STOCKS_BASE_URL + "/sector/1/automobile/" + FILTER_2);
        pullStockData(STOCKS_BASE_URL + "/sector/2/chemicals/" + FILTER_2);
        pullStockData(STOCKS_BASE_URL + "/sector/3/construction/" + FILTER_2);
        pullStockData(STOCKS_BASE_URL + "/sector/4/cons-durable/" + FILTER_2);
        pullStockData(STOCKS_BASE_URL + "/sector/5/fmcg/" + FILTER_2);
        pullStockData(STOCKS_BASE_URL + "/sector/6/diversified/" + FILTER_2);
        pullStockData(STOCKS_BASE_URL + "/sector/7/energy/" + FILTER_2);
        pullStockData(STOCKS_BASE_URL + "/sector/8/financial/" + FILTER_2);
        pullStockData(STOCKS_BASE_URL + "/sector/9/healthcare/" + FILTER_2);
        pullStockData(STOCKS_BASE_URL + "/sector/10/metals/" + FILTER_2);
        pullStockData(STOCKS_BASE_URL + "/sector/11/services/" + FILTER_2);
        pullStockData(STOCKS_BASE_URL + "/sector/12/technology/" + FILTER_2);
        pullStockData(STOCKS_BASE_URL + "/sector/13/textiles/" + FILTER_2);
        pullStockData(STOCKS_BASE_URL + "/sector/14/others/" + FILTER_2);
        pullStockData(STOCKS_BASE_URL + "/sector/15/engineering/" + FILTER_2);
        pullStockData(STOCKS_BASE_URL + "/sector/16/communication/" + FILTER_2);
        log.info("Completed!");
    }

    @Async
    @Transactional
    public void linkFundAndStock() throws IOException {
        List<Stock> stocks = stockRepo.findAllLatest();
        List<Portfolio> portfolios = portfolioRepo.findAllLatest();
        int i = 0;
        for (Stock stock: stocks) {
            i++;
            log.info("Processing: {} ,  {}", i, stock.getName());
            int mfCount = 0;
            Float mfSize = 0.0f;
            List<Object[]> summary = portfolioRepo.getAllByStockId(stock.getStockId());
            for(Object[] obj: summary) {
                mfSize = mfSize + ((Double) obj[0]).floatValue();
                mfCount++;
            }
            stock.setMfcount(mfCount);
            stock.setMfSize(mfSize);
        }
        log.info("Completed!");
    }

    private void pullMutualFundPortfolioData() throws IOException {
        List<Fund> funds = fundRepo.findAllLatest();
        List<Portfolio> result = new ArrayList<>();
        for(Fund fund: funds) {
            if(!fund.getCategory().startsWith("EQ")) {
                continue;
            }
            String urlStr = HOST + fund.getLink();
            log.info("Fetching Portfolio: {}", urlStr);
            String payload = getHtmlData(urlStr);
            Document doc = Jsoup.parse(payload);
            Elements mfItems = doc.select("table[id=equity-holdings-table]>tbody>tr");
            for (Element mfItem : mfItems) {
                Elements mfColumns = mfItem.select("td");
                Element aLink = mfColumns.get(1).select("a").first();
                String link = "";
                String stockId = "";
                if(aLink != null) {
                    link = aLink.attr("href").replaceAll("#snapshot", "");
                }
                if(!link.isEmpty()) {
                    stockId = link.split("/")[2];
                }

                Portfolio portfolio = new Portfolio();
                portfolio.setFund(fund.getName());
                portfolio.setFundId(fund.getFundId());
                portfolio.setCompany(mfColumns.get(1).text());
                portfolio.setLink(link);
                portfolio.setStockId(stockId);
                portfolio.setAllocation( Float.parseFloat((String) mfColumns.get(6).text()));
                portfolio.setSector(mfColumns.get(2).text());
                portfolio.setOnday(new Date());
                result.add(portfolio);
            }
        }
        portfolioRepo.saveAll(result);
        log.info("Completed, Items: {}", result.size());
    }

    private void pullMutualFundData(String urlStr) throws IOException {
        try {
            log.info("Fetching Fund: {}", urlStr);
            String payload = getHtmlData(urlStr);
            JsonNode htmlResponse = mapper.readTree(payload);
            String htmlData = htmlResponse.get("html_data").asText();
            Document doc = Jsoup.parse(htmlData);
            List<Fund> result = new ArrayList<>();
            Elements mfItems = doc.select("table>tbody>tr");
            int row = 0;
            for (Element mfItem : mfItems) {
                Elements mfColumns = mfItem.select("td");
                Object expense = mfColumns.get(6).text().replaceAll(",", "");
                Object returns = mfColumns.get(7).text().replaceAll(",", "");
                String name = mfColumns.get(1).text().split("\\|")[0].trim();
                String link = mfColumns.get(1).select("a").first().attr("href");
                String fundId = "";
                if (name.endsWith(" ")) {
                    name = name.substring(0, name.lastIndexOf(" "));
                }
                if(!link.isEmpty()) {
                    fundId = link.split("/")[2];
                }
                if (expense.toString().contains("-")) {
                    expense = "0.0";
                }
                if (returns.toString().contains("-")) {
                    returns = "0.0";
                }
                Object rank = mfColumns.get(8).text().split("/")[0];
                if (rank.toString().contains("-")) {
                    rank = "0";
                }
                Object asset = mfColumns.get(9).text().replaceAll(",", "");
                if (asset.toString().contains("-")) {
                    asset = "0.0";
                }

                Float assetCr = Float.parseFloat((String) asset);
                Float expenseRatio = Float.parseFloat((String) expense);
                String category = mfColumns.get(4).text().trim();
                Fund fund = new Fund();
                fund.setName(name);
                fund.setFundId(fundId);
                fund.setAsset(assetCr);
                fund.setExpenseRatio(expenseRatio);
                fund.setCategory(category);
                fund.setOnday(new Date());
                fund.setLink(link);
                result.add(fund);
            }
            fundRepo.saveAll(result);
            log.info("Completed, Items: {}", result.size());
        } catch (IOException ex) {
            log.error("IO Error {}", ex);
            throw ex;
        }
    }

    private void pullStockData(String urlStr) throws IOException {
        log.info("Fetching Stock: {}", urlStr);
        String category = urlStr.split("/")[7];
        List<Stock> result = new ArrayList<>();
        try {
            String payload = getHtmlData(urlStr);
            JsonNode htmlResponse = mapper.readTree(payload);
            JsonNode htmlDataNode = htmlResponse.get("data");
            for(JsonNode htmlNode: htmlDataNode) {
                String data = htmlNode.toString();
                int index1 = data.indexOf(">")+1;
                int index2 = data.indexOf("</a>");
                String data1 = data.substring(index2 + 4);
                int index3 = data1.indexOf("\">") + 2;
                int index4 = data1.indexOf("</a>");
                String data2 = data1.substring(index4 + 4);
                int index5 = data2.indexOf("\">") + 2;
                int index6 = data2.indexOf("</a>");
                String data3 = data2.substring(index6 + 2);
                int index7 = data3.indexOf("\",\"") + 3;
                int index8 = data3.indexOf("<small>");
                String data4 = data3.substring(index8);
                int index9 = data4.indexOf(">") + 1;
                int index10 = data4.indexOf("</");
                String data5 = data4.substring(index10+ 11);
                int index11 = data5.indexOf("\">") + 2;
                int index12 = data5.indexOf("</");
                int index14 = data.indexOf("/");
                int index15 = data.indexOf("\\\">");
                String data6 = data5.substring(index12 + 10).replaceAll("\",\"","|");
                String arr[] = data6.split("\\|");

                String name = data.substring(index1,index2);
                String link = data.substring(index14,index15);
                String stockId = "";
                if (!link.isEmpty()) {
                    stockId = link.split("/")[2];
                }
                String price = data3.substring(index7,index8).replaceAll(",","");
                String weekLow = arr[0].replaceAll(",","");
                if(weekLow.contains("-")) {
                    weekLow = "0";
                }
                String weekHigh = arr[1].replaceAll(",","");
                if(weekHigh.contains("-")) {
                    weekHigh = "0";
                }
                String mcap = arr[2].replaceAll(",","");
                if(mcap.contains("-")) {
                    mcap = "0";
                }
                String value = arr[3].replaceAll(",","");
                if(value.contains("-")) {
                    value = "0";
                }
                String type = data1.substring(index3, index4);
                String sub = data2.substring(index5,index6);
                String onDate =  data4.substring(index9,index10);
                String perChange = data5.substring(index11,index12);

                Stock stock = new Stock();
                stock.setName(name);
                stock.setLink(link);
                stock.setStockId(stockId);
                stock.setPrice(Float.parseFloat((String) price ));
                stock.setWeeklow(Float.parseFloat((String) weekLow ));
                stock.setWeekhigh(Float.parseFloat((String) weekHigh));
                stock.setValue(Float.parseFloat((String) value ));
                stock.setMcap(Float.parseFloat((String) mcap ));
                stock.setCategory(category);
                stock.setOnday(new Date());
                stock.setMfSize(0.0f);
                stock.setMfcount(0);
                result.add(stock);
            }
            stockRepo.saveAll(result);
            log.info("Completed, Items: {}", result.size());
        } catch (IOException ex) {
            log.error("IO Error {}", ex);
            throw ex;
        }
    }

    private String getHtmlData(String urlStr) throws IOException {
        URL url = new URL(urlStr);
        URLConnection conn = url.openConnection();
        conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11"
                + " (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
        conn.connect();
        String inputLine;
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        while ((inputLine = br.readLine()) != null) {
            sb.append(inputLine);
        }
        return sb.toString();
    }
}
