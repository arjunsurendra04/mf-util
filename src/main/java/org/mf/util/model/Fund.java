package org.mf.util.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;

@Entity(name = "fund")
@Data
public class Fund {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String fundId;
  private String name;
  private Float asset;
  private Float expenseRatio;
  private String category;
  private String link;
  private Date onday;
}
