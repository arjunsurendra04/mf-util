package org.mf.util.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "stock")
@Data
public class Stock {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String stockId;
    private String name;
    private Float price;
    private Float weekhigh;
    private Float weeklow;
    private Float pe;
    private Float pb;
    private Float value;
    private Float mcap;
    private String category;
    private String link;
    private Date onday;
    private Integer mfcount;
    private Float mfSize;
}
