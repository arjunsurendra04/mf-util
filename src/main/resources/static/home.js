$(document).ready(function () {
    var lineChartOptions = {
        chart: {
            renderTo: 'chart1'
        },
        title: {
            text: 'Asset Size',
        },
        subtitle: {
            text: 'Subtitle',
        },
        xAxis: {
            categories: []
        },
        yAxis: {
            title: {
                text: 'Requests'
            },
            plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
        },
        tooltip: {
            valueSuffix: 'Cr'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: []
    };

    $('#sel1').on('change', function () {
        var fund = escape(this.value.trim());
        $.getJSON("/ust-data?fund=" + fund, function (json) {
            lineChartOptions.xAxis.categories = json[0]['data'];
            for (i = 1; i < Object.keys(json).length; i++) {
                lineChartOptions.series[i - 1] = json[i];
            }
            chart = new Highcharts.Chart(lineChartOptions);
        });
    })
});