$(document).ready( function () {
    $('#stock-table').DataTable(
        {
            "bFilter": false,
            "bPaginate": false,
            "order": [[ 0, "asc" ]]
        }
    );
} );