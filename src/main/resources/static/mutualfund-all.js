$(document).ready( function () {
    $('#fund-table').DataTable(
        {
            "bFilter": false,
            "bPaginate": false,
            "order": [[ 0, "asc" ]]
        }
    );
} );